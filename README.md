This is a simple project to test the new feature deliverd in v12.7 on Jan. 22, 2020 to disable DinD for Dependency and SAST security scans.

Refer to the .gitlab-ci.yml to review the variable settings and Dependency and SAST includes.